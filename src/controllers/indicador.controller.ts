import { Request, Response } from 'express';
import { Handler } from 'express';
import axios from 'axios';
import Indicador, { IUser } from '../models/Indicador';



export const indicador: Handler = async(req, res) => {
    try {

        const indicador = await (await axios.get('https://mindicador.cl/api')).data;
     



        console.log("resultado", indicador);
        res.json(indicador);


    }catch(err) {

        console.log(err);

    }
};

export const indicadorFecha: Handler = async(req, res) => {
    try {
        
        const { tipo_indicador , fecha} = req.body;

        const indicador = await (await axios.post(`https://mindicador.cl/api/${tipo_indicador}/${fecha}`)).data;
     
          console.log("info", tipo_indicador, fecha);
        console.log("resultado", indicador);
        res.json({indicador});


    }catch(err) {

        console.log(err);

    }
};









