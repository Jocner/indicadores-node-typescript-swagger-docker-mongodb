import { Router } from 'express';
const router = Router();
import { indicador, indicadorFecha } from '../controllers/indicador.controller';



/**
 * @swagger
 * components:
 *  schemas:
 *    Indicador:
 *      type: object
 *      properties:
 *        :
 *          type: string
 *          description: the auto-generated id of indicador
 *        Indicador:
 *          type: object
 *          properties:
 *            version:
 *              type: string
 *              description: version
 *            autor:
 *              type: string
 *              description: mindicador.cl
 *            fecha:
 *              type: date
 *              description: 12-02-2021
 *            uf:
 *              type: object
 *              properties:
 *                codigo:
 *                  type: string
 *                  description: uf
 *                nombre: 
 *                  type: string
 *                  description: Unidad de fomento (UF)
 *                unidad_medida: 
 *                  type: string
 *                  description: Pesos
 *                fecha: 
 *                  type: date
 *                  description: 15-07-2021
 *                valor: 
 *                  type: number
 *                  description: 29255.22
 *      example:
 *        version: 1.0.2
 *        autor: mindicador.cl
 *        fecha: 12-02-2021
 *        uf: []
 *
 *    Indicadorfecha:
 *      type: object
 *      properties:
 *        tipo_indicador: 
 *          type: string
 *          description: uf
 *        fecha:
 *          type: date
 *          description: 10-05-2021
 *      example:
 *        tipo_indicador: uf
 *        fecha: 10-05-2021 
 * 
 *    IndicadorNotFound:
 *      type: object
 *      properties:
 *        msg:
 *          type: string
 *          description: A message for the not found user
 *      example:
 *        msg: User was not found  
 * 
 *  parameters:
 *    userId:
 *      in: path
 *      name: id
 *      required: true
 *      schema:
 *        type: string
 *      description: the user id 
 */

/**
 * @swagger
 * tags:
 *  name: Incadores
 *  description: Indicadores Economicos endpoint
 */


/**
 * @swagger
 * /indicadores:
 *  get:
 *    summary: Return indicadores economicos list
 *    tags: [Incadores]
 *    responses:
 *      200:
 *        description: list of indicadores
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Indicador'      
 */
 router.get('/indicadores', 
 indicador
);

/**
 * @swagger
 * /indicadores-fecha:
 *  post:
 *    summary: create a new date indicador
 *    tags: [Indicadores]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Indicadorfecha'
 *    responses:
 *      200:
 *        description: the users was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Indicador' 
 *      500:
 *        description: Some server error
 *
 */
 router.post('/indicadores-fecha',
 indicadorFecha
);



export default router;
   