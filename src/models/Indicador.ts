import { Schema, model, Document } from 'mongoose';


const usersSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    rut: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    celular: {
        type: String,
        require: true,
        trim: true
    }
});

// const usersSchema = new Schema({
//     name: String,
//     rut: String,
//     email: String,
//     celular: String
// });

export interface IUser extends Document {
    name: string;
    rut: string;
    email: string;
    celular: string;
}



export default model<IUser>('Indicador', usersSchema);

