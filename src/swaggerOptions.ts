export const option = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'Indicadores API',
            version: '1.0.0',
            description: "API rest"
        },
        servers: [
            {
                url: "http://localhost:9000"
            }
        ]
    },
    apis: ["./src/routes/*.ts"]
}